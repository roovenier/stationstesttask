//
//  StationDetailController.swift
//  StationsTestTask
//
//  Created by alexander.oschepkov on 25.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit
import MapKit

class StationDetailController: UIViewController {
    
    @IBOutlet weak var stationTitle: UILabel!
    @IBOutlet weak var stationAddress: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var station: StationModel? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = self.station?.stationTitle
        
        self.stationTitle.text = self.station?.stationTitle
        
        var stationAddressArray: [String] = [(self.station?.countryTitle)!, (self.station?.regionTitle)!, (self.station?.districtTitle)! , (self.station?.cityTitle)!];
        stationAddressArray = stationAddressArray.filter() { $0 != "" }
        
        self.stationAddress.text = stationAddressArray.joined(separator: ", ")
        
        let annotation = MKPointAnnotation()
        let centerCoordinate = CLLocationCoordinate2D(latitude: self.station?.point["latitude"] as! CLLocationDegrees, longitude: self.station?.point["longitude"] as! CLLocationDegrees)
        annotation.coordinate = centerCoordinate
        annotation.title = self.station?.stationTitle
        self.mapView.addAnnotation(annotation)
        
        let viewRegion = MKCoordinateRegionMakeWithDistance(centerCoordinate, 200, 200)
        self.mapView.setRegion(viewRegion, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
