//
//  ScheduleController.swift
//  StationsTestTask
//
//  Created by Александр Ощепков on 18.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

class ScheduleController: UIViewController, UITableViewDelegate, SelectControllerDelegate {

    @IBOutlet weak var containerView: UIView!
    var tableView: UITableView!
    var datePicker = UIDatePicker()
    
    var departureCountryValue = String()
    var departureCityValue = String()
    var departureStation: StationModel? = nil
    
    var arrivalCountryValue = String()
    var arrivalCityValue = String()
    var arrivalStation: StationModel? = nil
    
    var stationDictionary: [String: String] = [:];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView = containerView.subviews.first as! UITableView
        self.tableView?.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let datePicker = self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 2))?.contentView.subviews.first as? UIDatePicker {
            self.datePicker = datePicker
            self.datePicker.addTarget(self, action: #selector(dateIsChanged(sender:)), for: .valueChanged)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    func dateStringFromDateObject(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        return dateFormatter.string(from: date)
    }
    
    func dateIsChanged(sender: UIDatePicker) {
        self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 2))?.textLabel?.text = self.dateStringFromDateObject(date: sender.date)
    }
    
    func checkStationsSelected() {
        if (self.departureStation != nil) && (self.arrivalStation != nil) {
            self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 2))?.textLabel?.isEnabled = true
            self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 2))?.isUserInteractionEnabled = true
            self.datePicker.alpha = 1
        }
    }
    
    func departureResetCityAndStation() {
        self.departureCityValue = ""
        self.tableView.cellForRow(at: IndexPath.init(item: 1, section: 0))?.textLabel?.text = "Выберите город"
        
        self.departureStation = nil
        let stationCell = self.tableView.cellForRow(at: IndexPath.init(item: 2, section: 0))
        stationCell?.textLabel?.text = "Выберите станцию"
        stationCell?.isUserInteractionEnabled = false
        stationCell?.textLabel?.isEnabled = false
        stationCell?.accessoryType = .disclosureIndicator
        
        self.datePickerDisable()
    }
    
    func departureResetStation() {
        self.departureStation = nil
        
        let cell = self.tableView.cellForRow(at: IndexPath.init(item: 2, section: 0))
        
        cell?.textLabel?.text = "Выберите станцию"
        cell?.accessoryType = .disclosureIndicator
        
        self.datePickerDisable()
    }
    
    func arrivalResetCityAndStation() {
        self.arrivalCityValue = ""
        self.tableView.cellForRow(at: IndexPath.init(item: 1, section: 1))?.textLabel?.text = "Выберите город"
        
        self.arrivalStation = nil
        let stationCell = self.tableView.cellForRow(at: IndexPath.init(item: 2, section: 1))
        stationCell?.textLabel?.text = "Выберите станцию"
        stationCell?.isUserInteractionEnabled = false
        stationCell?.textLabel?.isEnabled = false
        stationCell?.accessoryType = .disclosureIndicator
        
        self.datePickerDisable()
    }
    
    func arrivalResetStation() {
        self.arrivalStation = nil
        
        let cell = self.tableView.cellForRow(at: IndexPath.init(item: 2, section: 1))
        
        cell?.textLabel?.text = "Выберите станцию"
        cell?.accessoryType = .disclosureIndicator
        
        self.datePickerDisable()
    }
    
    func datePickerDisable() {
        let dateLabel = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 2))?.textLabel
        dateLabel?.text = "Выберите дату"
        dateLabel?.isEnabled = false
        
        self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 2))?.isUserInteractionEnabled = false
        self.datePicker.alpha = 0.3
    }
    
    func makeCellDisabled(forRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath)
        
        cell!.isUserInteractionEnabled = false
        cell!.textLabel?.isEnabled = false
    }
    
    func makeCellEnabled(forRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath)
        
        cell!.isUserInteractionEnabled = true
        cell!.textLabel?.isEnabled = true
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 && indexPath.row == 1 {
            return 216;
        }
        
        return 44;
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StationDetailController") as? StationDetailController
        
        if indexPath.section == 0 {
            vc?.station = self.departureStation
        } else if indexPath.section == 1 {
            vc?.station = self.arrivalStation
        }
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 2 {
            let title = self.tableView.cellForRow(at: indexPath)?.textLabel?.text
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectController") as! SelectController
            vc.controllerTitle = title!;
            vc.delegate = self
            
            if indexPath.section == 0 {
                vc.controllerMovement = SelectController.MovementType.departure
                
                if indexPath.row == 0 {
                    vc.controllerType = SelectController.EntityType.countries
                } else if indexPath.row == 1 {
                    vc.controllerType = SelectController.EntityType.cities
                    vc.countrySelected = self.departureCountryValue
                } else if indexPath.row == 2 {
                    vc.controllerType = SelectController.EntityType.stations
                    vc.citySelected = self.departureCityValue
                }
            } else if indexPath.section == 1 {
                vc.controllerMovement = SelectController.MovementType.arrival
                
                if indexPath.row == 0 {
                    vc.controllerType = SelectController.EntityType.countries
                } else if indexPath.row == 1 {
                    vc.controllerType = SelectController.EntityType.cities
                    vc.countrySelected = self.arrivalCountryValue
                } else if indexPath.row == 2 {
                    vc.controllerType = SelectController.EntityType.stations
                    vc.citySelected = self.arrivalCityValue
                }
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: SelectControllerDelegate
    
    func setDepartureCountry(cellValue value: String) {
        let indexPath = IndexPath.init(item: 0, section: 0)
        self.tableView.cellForRow(at: indexPath)?.textLabel?.text = value
        _ = self.navigationController?.popViewController(animated: true)
        self.makeCellEnabled(forRowAt: IndexPath.init(item: 1, section: 0))
        
        if(value != self.departureCountryValue) {
            self.departureResetCityAndStation()
        }
        
        self.departureCountryValue = value
    }
    
    func setDepartureCity(cellValue value: String) {
        let indexPath = IndexPath.init(item: 1, section: 0)
        self.tableView.cellForRow(at: indexPath)?.textLabel?.text = value
        _ = self.navigationController?.popViewController(animated: true)
        self.makeCellEnabled(forRowAt: IndexPath.init(item: 2, section: 0))
        
        if(value != self.departureCityValue) {
            self.departureResetStation()
        }
        
        self.departureCityValue = value
    }
    
    func setDepartureStation(stationDictionary value: StationModel) {
        let indexPath = IndexPath.init(item: 2, section: 0)
        self.departureStation = value
        
        let cell = self.tableView.cellForRow(at: indexPath)
        
        cell?.textLabel?.text = value.stationTitle
        cell?.accessoryType = .detailDisclosureButton
        _ = self.navigationController?.popViewController(animated: true)
        
        self.checkStationsSelected()
    }
    
    func setArrivalCountry(cellValue value: String) {
        let indexPath = IndexPath.init(item: 0, section: 1)
        self.tableView.cellForRow(at: indexPath)?.textLabel?.text = value
        _ = self.navigationController?.popViewController(animated: true)
        self.makeCellEnabled(forRowAt: IndexPath.init(item: 1, section: 1))
        
        if(value != self.arrivalCountryValue) {
            self.arrivalResetCityAndStation()
        }
        
        self.arrivalCountryValue = value
    }
    
    func setArrivalCity(cellValue value: String) {
        let indexPath = IndexPath.init(item: 1, section: 1)
        self.tableView.cellForRow(at: indexPath)?.textLabel?.text = value
        _ = self.navigationController?.popViewController(animated: true)
        self.makeCellEnabled(forRowAt: IndexPath.init(item: 2, section: 1))
        
        if(value != self.arrivalCityValue) {
            self.arrivalResetStation()
        }
        
        self.arrivalCityValue = value
    }
    
    func setArrivalStation(stationDictionary value: StationModel) {
        let indexPath = IndexPath.init(item: 2, section: 1)
        self.arrivalStation = value
        
        let cell = self.tableView.cellForRow(at: indexPath)
        
        cell?.textLabel?.text = value.stationTitle
        cell?.accessoryType = .detailDisclosureButton
        _ = self.navigationController?.popViewController(animated: true)
        
        self.checkStationsSelected()
    }
    
    
    @IBAction func aboutAction(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutController")
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
