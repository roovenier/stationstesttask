//
//  SelectController.swift
//  StationsTestTask
//
//  Created by Александр Ощепков on 19.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

protocol SelectControllerDelegate {
    func setDepartureCountry(cellValue value: String)
    func setDepartureCity(cellValue value: String)
    func setDepartureStation(stationDictionary value: StationModel)
    func setArrivalCountry(cellValue value: String)
    func setArrivalCity(cellValue value: String)
    func setArrivalStation(stationDictionary value: StationModel)
}

class SelectController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    enum EntityType: Int {
        case countries = 1
        case cities = 2
        case stations = 3
    }
    
    enum MovementType: Int {
        case departure = 1
        case arrival = 2
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: SelectControllerDelegate?
    
    var controllerTitle = String()
    var controllerType = EntityType.countries
    var controllerMovement = MovementType.departure
    
    var countrySelected = String()
    var citySelected = String()
    
    var entitiesArray = [String]()
    var entitiesArrayFull = [String]()
    
    var entitiesArrayOfStationObjects = [StationModel]()
    var entitiesArrayOfStationObjectsFull = [StationModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.controllerTitle;
        
        if self.controllerMovement == MovementType.departure {
            switch self.controllerType {
            case EntityType.countries:
                self.entitiesArray = self.departureCountriesFromData()
            case EntityType.cities:
                self.entitiesArray = self.departureCitiesFromData()
            case EntityType.stations:
                self.entitiesArray = self.departureStationsFromData()
            }
        } else if self.controllerMovement == MovementType.arrival {
            switch self.controllerType {
            case EntityType.countries:
                self.entitiesArray = self.arrivalCountriesFromData()
            case EntityType.cities:
                self.entitiesArray = self.arrivalCitiesFromData()
            case EntityType.stations:
                self.entitiesArray = self.arrivalStationsFromData()
            }
        }
        
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    func departureCountriesFromData() -> Array<String> {
        var countriesArray = [String]()
        
        do {
            if let file = Bundle.main.url(forResource: "allStations", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let countries = json?["citiesFrom"] as? [[String: Any]] {
                    for country in countries {
                        if let countryTitle = country["countryTitle"] as? String {
                            if countriesArray.index(of: countryTitle) == nil {
                                countriesArray.append(countryTitle)
                            }
                        }
                    }
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        self.entitiesArrayFull = countriesArray
        
        return countriesArray
    }
    
    func departureCitiesFromData() -> Array<String> {
        var citiesArray = [String]()
        
        do {
            if let file = Bundle.main.url(forResource: "allStations", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let countries = json?["citiesFrom"] as? [[String: Any]] {
                    for country in countries {
                        if let countryTitle = country["countryTitle"] as? String, let cityTitle = country["cityTitle"] as? String {
                            if countryTitle == self.countrySelected {
                                citiesArray.append(cityTitle)
                            }
                        }
                    }
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        self.entitiesArrayFull = citiesArray
        
        return citiesArray
    }
    
    func departureStationsFromData() -> Array<String> {
        var stationsArray = [String]()
        var stationsArrayOfDictionary = [StationModel]()
        
        do {
            if let file = Bundle.main.url(forResource: "allStations", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let countries = json?["citiesFrom"] as? [[String: Any]] {
                    for country in countries {
                        let stations = country["stations"] as? [[String: Any]]
                        let cityTitle = country["cityTitle"] as? String
                        
                        if cityTitle == self.citySelected {
                            for station in stations! {
                                stationsArrayOfDictionary.append(StationModel.init(dictionary: station))
                                if let stationTitle = station["stationTitle"] as? String {
                                    stationsArray.append(stationTitle)
                                }
                            }
                        }
                    }
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        self.entitiesArrayFull = stationsArray
        
        self.entitiesArrayOfStationObjectsFull = stationsArrayOfDictionary
        self.entitiesArrayOfStationObjects = stationsArrayOfDictionary
        
        return stationsArray
    }
    
    func arrivalCountriesFromData() -> Array<String> {
        var countriesArray = [String]()
        
        do {
            if let file = Bundle.main.url(forResource: "allStations", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let countries = json?["citiesTo"] as? [[String: Any]] {
                    for country in countries {
                        if let countryTitle = country["countryTitle"] as? String {
                            if countriesArray.index(of: countryTitle) == nil {
                                countriesArray.append(countryTitle)
                            }
                        }
                    }
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        self.entitiesArrayFull = countriesArray
        
        return countriesArray
    }
    
    func arrivalCitiesFromData() -> Array<String> {
        var citiesArray = [String]()
        
        do {
            if let file = Bundle.main.url(forResource: "allStations", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let countries = json?["citiesTo"] as? [[String: Any]] {
                    for country in countries {
                        if let countryTitle = country["countryTitle"] as? String, let cityTitle = country["cityTitle"] as? String {
                            if countryTitle == self.countrySelected {
                                citiesArray.append(cityTitle)
                            }
                        }
                    }
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        self.entitiesArrayFull = citiesArray
        
        return citiesArray
    }
    
    func arrivalStationsFromData() -> Array<String> {
        var stationsArray = [String]()
        var stationsArrayOfDictionary = [StationModel]()
        
        do {
            if let file = Bundle.main.url(forResource: "allStations", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let countries = json?["citiesTo"] as? [[String: Any]] {
                    for country in countries {
                        let stations = country["stations"] as? [[String: Any]]
                        let cityTitle = country["cityTitle"] as? String
                        
                        if cityTitle == self.citySelected {
                            for station in stations! {
                                stationsArrayOfDictionary.append(StationModel.init(dictionary: station))
                                if let stationTitle = station["stationTitle"] as? String {
                                    stationsArray.append(stationTitle)
                                }
                            }
                        }
                    }
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        self.entitiesArrayFull = stationsArray
        
        self.entitiesArrayOfStationObjectsFull = stationsArrayOfDictionary
        self.entitiesArrayOfStationObjects = stationsArrayOfDictionary
        
        return stationsArray
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.entitiesArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if(cell == nil) {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
        }
        
        let cellValue = self.entitiesArray[indexPath.row]
        
        cell?.textLabel?.text = cellValue
        
        return cell!;
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            func filterContentForSearchText(searchText: String) -> Array<String> {
                return self.entitiesArrayFull.filter { term in
                    return term.lowercased().contains(searchText.lowercased())
                }
            }
            
            self.entitiesArray = filterContentForSearchText(searchText: searchText)
            
            
            if self.entitiesArrayOfStationObjects.count > 0 {
                func filterContentForSearchText(searchText: String) -> Array<StationModel> {
                    return self.entitiesArrayOfStationObjectsFull.filter { station in
                        return station.stationTitle.lowercased().contains(searchText.lowercased())
                    }
                }
                
                self.entitiesArrayOfStationObjects = filterContentForSearchText(searchText: searchText)
            }
            
            self.tableView.reloadData()
        } else {
            self.entitiesArray = self.entitiesArrayFull
            
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellValue = self.entitiesArray[indexPath.row]
        
        if self.controllerMovement == MovementType.departure {
            switch self.controllerType {
            case EntityType.countries:
                self.delegate?.setDepartureCountry(cellValue: cellValue)
            case EntityType.cities:
                self.delegate?.setDepartureCity(cellValue: cellValue)
            case EntityType.stations:
                let stationDictionary = self.entitiesArrayOfStationObjects[indexPath.row] as StationModel
                self.delegate?.setDepartureStation(stationDictionary: stationDictionary)
            }
        } else if self.controllerMovement == MovementType.arrival {
            switch self.controllerType {
            case EntityType.countries:
                self.delegate?.setArrivalCountry(cellValue: cellValue)
            case EntityType.cities:
                self.delegate?.setArrivalCity(cellValue: cellValue)
            case EntityType.stations:
                let stationDictionary = self.entitiesArrayOfStationObjects[indexPath.row] as StationModel
                self.delegate?.setArrivalStation(stationDictionary: stationDictionary)
            }
        }
    }
}
