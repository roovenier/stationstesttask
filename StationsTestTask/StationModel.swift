//
//  StationModel.swift
//  StationsTestTask
//
//  Created by alexander.oschepkov on 25.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

import UIKit

class StationModel: NSObject {

    var stationTitle = String()
    var countryTitle = String()
    var districtTitle = String()
    var regionTitle = String()
    var cityTitle = String()
    var point = NSDictionary()
    
    init(dictionary: [String: Any]) {
        self.stationTitle = dictionary["stationTitle"]! as! String
        self.countryTitle = dictionary["countryTitle"]! as! String
        self.districtTitle = dictionary["districtTitle"]! as! String
        self.regionTitle = dictionary["regionTitle"]! as! String
        self.cityTitle = dictionary["cityTitle"]! as! String
        self.point = (dictionary["point"] as? NSDictionary)!
    }
}
